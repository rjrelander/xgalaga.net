/* $Id: score.c,v 1.2 1998/04/30 05:11:58 mrogre Exp $ */
/* Copyright (c) 1998 Joe Rumsey (mrogre@mediaone.net) */
#include "copyright.h"

#include <config.h>
#include <stdio.h>
#include "Wlib.h"
#include "images.h"
#include "data.h"
#include "defs.h"
#include "struct.h"
#include "proto.h"

char scorestr[40] = "Score: 0000000", shipstr[4] ="";
W_Image *miniship, *extraImage;
int drawExtra = 0, extrax, extray;

void undo_score()
{
    int basex;

    basex = WINWIDTH/2 - ((strlen(scorestr)/2)*W_Textwidth);
    W_ClearArea(baseWin, basex, 0, strlen(scorestr) * W_Textwidth + 1, W_Textheight + 1);
    W_ClearArea(baseWin, 0, 0, (miniship->width+2)*6, miniship->height);
    basex = 6*(miniship->width+2) + 2;
    W_ClearArea(baseWin, basex, 0, strlen(shipstr) * W_Textwidth + 1, W_Textheight + 1);
    if(drawExtra)
	W_ClearArea(baseWin, extrax-(extraImage->width/2), extray-(extraImage->height/2), 
		    extraImage->width, extraImage->height);
}

void do_score()
{
    int basex;
    int i;
    static int lastscore;

    sprintf(scorestr, "Score: %07d     Level: %02d", score, level);
    basex = WINWIDTH/2 - ((strlen(scorestr)/2)*W_Textwidth);
    W_MaskText(baseWin, basex, 1, W_Grey, scorestr, strlen(scorestr), W_RegularFont);
    W_MaskText(baseWin, basex+1, 0, W_Yellow, scorestr, strlen(scorestr), W_RegularFont);

    for(i=0;i<((ships < 6) ? ships : 6);i++) {
	W_DrawImage(baseWin, i*(miniship->width+2), 0, 0, miniship, W_White);
    }
    if(ships>6) {
	sprintf(shipstr, "%d", ships);
	basex = 6*(miniship->width+2) + 2;
	W_MaskText(baseWin, basex, 1, W_Grey, shipstr, strlen(shipstr), W_RegularFont);
	W_MaskText(baseWin, basex+1, 0, W_Yellow, shipstr, strlen(shipstr), W_RegularFont);
    }
    if(lastscore != score) {
	if((score > 0) && (score >= nextBonus)) {
	    ships++;
	    extrax = 0 - extraImage->width/2;
	    extray = WINHEIGHT/2;
	    drawExtra = 1;
	    if(nextBonus < BONUSSHIPSCORE)
		nextBonus = BONUSSHIPSCORE;
	    else
		nextBonus += BONUSSHIPSCORE;
	}
	lastscore=score;
    }
    if(drawExtra) {
	extrax += 10;
	W_DrawImage(baseWin, extrax-(extraImage->width/2), extray-(extraImage->height/2), 0, extraImage, W_White);
	if((extrax-(int)extraImage->width/2) > WINWIDTH)
	    drawExtra = 0;
    }
}

void init_score()
{
    miniship = getImage(I_MINISHIP);
    extraImage = getImage(I_EXTRA);
}

