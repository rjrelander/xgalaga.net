/* $Id: title.c,v 1.4 1998/05/11 06:52:59 mrogre Exp $ */
/* Copyright (c) 1998 Joe Rumsey (mrogre@mediaone.net) */
#include "copyright.h"
#include <config.h>

#include <stdio.h>
#include "Wlib.h"
#include "defs.h"
#include "images.h"
#include "data.h"
#include "struct.h"
#include "proto.h"

W_Image *pauseImage, *titleImage;
int pausex, pausey, pauseyspeed=1;

void undo_pause()
{
    W_CacheClearArea(baseWin, pausex-(pauseImage->width/2), pausey-(pauseImage->height/2), 
		     pauseImage->width, pauseImage->height);
}

void do_pause()
{
    if((pausey < pauseImage->height/2) || ((pausey+(pauseImage->height/2)) >= WINHEIGHT))
	pauseyspeed = -pauseyspeed;
    pausey+=pauseyspeed;

    W_DrawImage(baseWin, pausex-(pauseImage->width/2), pausey-(pauseImage->height/2), 0, pauseImage, W_White);
}

void 
center_text(char *text, int y, W_Color color)
{
/*
    W_MaskText(baseWin, WINWIDTH/2 - ((strlen(text)*W_Textwidth)/2)-1, y+1, W_Black,
	       text, strlen(text), W_RegularFont);
*/
    W_MaskText(baseWin, WINWIDTH/2 - ((strlen(text)*W_Textwidth)/2), y, color,
	       text, strlen(text), W_RegularFont);
}

#if 0 /* ships vary from level to level now */
static void show_points()
{
    int i, ty;
    int a_images[6] = {
	I_ALIEN1,
	I_ALIEN2,
	I_ALIEN3,
	I_ALIEN4,
	I_ALIEN5,
	I_ALIEN6
    };

    char buf[40];

    for (i=0;i<6;i++) {
	W_DrawImage(baseWin, WINWIDTH/2 - 30, 220+i*21,
		    0, getImage(a_images[5-i]), W_Red);
	if(i < 5)
	    sprintf(buf, "- %d", (i+1)*100);
	else
	    sprintf(buf, "- ???");
	W_MaskText(baseWin, WINWIDTH/2, 225+i*21,
		   W_Yellow, buf, strlen(buf), W_RegularFont);
    }
    ty = 220+i*21;
    center_text("Ships in convoy are worth 50 points", ty, W_Green); ty += W_Textheight;
    center_text("Bonus ships at 20,000, 50,000, then every 50,000.", ty, W_Green);
}
#endif

static void show_help()
{
    int top = 220;

    center_text("         Keyboard controls           ", top, W_Red);
    center_text("  left/right arrow   move            ", top+10, W_Yellow);
    center_text("  space bar          fire            ", top+20, W_Yellow);
    center_text("  p                  pause           ", top+30, W_Cyan);
    center_text("  q                  end game        ", top+40, W_Cyan);
    center_text("  Q                  quick quit      ", top+50, W_Cyan);
    center_text("  k                  keyboard control", top+60, W_Cyan);
    center_text("  m                  mouse control   ", top+70, W_Cyan);
#ifdef SOUND
    center_text("  s                  toggle sound    ", top+80, W_Cyan);
#endif
	  
    center_text("Bonus ships at 20,000, 50,000, then every 50,000.", top+100, W_Green);
    center_text("XGalaga Home page: http://ogresoft.dyn.ml.org/xgalaga.html",
		top+150, W_Cyan);
}
void do_title()
{
    int ty;
    char vbuf[10];
    W_DrawImage(baseWin, WINWIDTH/2 - titleImage->width/2, 50, 0, titleImage, W_Red);
    sprintf(vbuf, "v%s", VERSION);
    center_text(vbuf, 40+titleImage->height, W_Yellow);

    ty = 60 + titleImage->height;
    if(getting_name)
	title_page = 0;

    switch(title_page) {
      case 0:
	center_text("Copyright (c) 1995-1998   Joe Rumsey", ty, W_Green); ty+= W_Textheight;
	center_text("<mrogre@mediaone.net>", ty, W_Green); ty+= 2*W_Textheight;

	center_text("This game is free software, covered by the GPL", ty, W_Yellow); ty += W_Textheight;
	center_text("If you enjoy it, visit http://www.gnu.org and help", ty, W_Yellow); ty+= W_Textheight;
	center_text("support the Free Software Foundation", ty, W_Yellow);ty+= W_Textheight;

	if(!getting_name) {
	    show_help();
	}
	break;
      case 1:
      default:
	show_scores();
	break;
    }

    center_text("Press k for keyboard control, m for mouse", WINHEIGHT - 2*W_Textheight, W_Yellow);
    center_text("Or q to quit", WINHEIGHT - W_Textheight, W_Yellow);
    pagetimer--;
    if(!pagetimer) {
	W_ClearWindow(baseWin);
	title_page++;
	if(title_page > 1)
	    title_page = 0;
	pagetimer = 300;
    }
}

void init_titles()
{
    titleImage = getImage(I_TITLE);

    pauseImage = getImage(I_PAUSE);
    pausex = WINWIDTH/2;
    pausey = WINHEIGHT/2;
    pauseyspeed = 3;
}



