#include "allincludes.h"

#define tiny_width 3
#define tiny_height 3
#define tiny_x_hot 1
#define tiny_y_hot 1
static unsigned char tiny_bits[] = {
  0x05, 0x02, 0x05, };
static unsigned char tinymask_bits[] = {
  0x05, 0x02, 0x05, };

#define cross_width 16
#define cross_height 16
#define cross_x_hot 7
#define cross_y_hot 7
static unsigned char cross_bits[] = {
    0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0xc0, 0x01, 0x80, 0x00,
    0x10, 0x04, 0x3f, 0x7e, 0x10, 0x04, 0x80, 0x00, 0xc0, 0x01, 0x80, 0x00,
0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00};
static unsigned char crossmask_bits[] = {
    0xc0, 0x01, 0xc0, 0x01, 0xc0, 0x01, 0xc0, 0x01, 0xe0, 0x03, 0xd0, 0x05,
    0xbf, 0x7e, 0x7f, 0x7f, 0xbf, 0x7e, 0xd0, 0x05, 0xe0, 0x03, 0xc0, 0x01,
0xc0, 0x01, 0xc0, 0x01, 0xc0, 0x01, 0x00, 0x00};

static void
checkGeometry(name, x, y, width, height)
    char   *name;
    int    *x, *y, *width, *height;
/* fixed so that it handles negative positions 12/21/93          */
/* note that this is NOT standard X syntax, but it was requested */
/* and it's how BRM-Hadley works.                       [BDyess] */
{
    char   *adefault;
    char    buf[100];
    char   *s;

#ifdef RJC
    *x = *y = INVALID_POSITION;
#endif				/* RJC */

    sprintf(buf, "%s.geometry", name);
    adefault = NULL; /*stringDefault(buf,NULL);*/
    if (adefault == NULL)
	return;
    /* geometry should be of the form 502x885+1+1, 502x885, or +1+1 */
    s = adefault;
    if (*s != '+' && *s != '-') {
	while (*s != 'x' && *s != 0)
	    s++;
	*width = atoi(adefault);
	if (*s == 0)
	    return;
	s++;
	adefault = s;
	while (*s != '+' && *s != '-' && *s != 0)
	    s++;
	*height = atoi(adefault);
	if (*s == 0)
	    return;
    }
    adefault = s;
    s++;
    if (*s == '-')
	s++;			/* for the case where they have wxh+-x+y */
    while (*s != '+' && *s != '-' && *s != 0)
	s++;
    *x = atoi(adefault + 1);
    if (*adefault == '-')
	*x = -*x;
    if (*s == 0)
	return;
    *y = atoi(s + 1);
    if (*s == '-')
	*y = -*y;
    /* printf("width: %d, height: %d, x: %d, y: %d\n",*width, *height,*x,*y); */
    return;
}

static  void
checkParent(name, parent)
    char   *name;
    W_Window *parent;
{
    char   *adefault;
    char    buf[100];
    int     i;
    struct windowlist *windows;

    sprintf(buf, "%s.parent", name);
    adefault = NULL; /*stringDefault(buf,NULL);*/
    if (adefault == NULL)
	return;
    /* parent must be name of other window or "root" */
    if (strcasecmp(adefault, "root") == 0) {
	*parent = W_Window2Void(&myroot);
	return;
    }
    for (i = 0; i < HASHSIZE; i++) {
	windows = hashtable[i];
	while (windows != NULL) {
	    if (strcasecmp(adefault, windows->window->name) == 0) {
		*parent = W_Window2Void(windows->window);
		return;
	    }
	    windows = windows->next;
	}
    }
}

static void
checkCursor(name, cursname, cursor)
    char   *name;
    char   *cursname;
    Cursor *cursor;
{
    char    buf[100];
    char   *adefault;

    *cursor = 0;

    sprintf(buf, "%s.cursor", name);
    adefault = cursname; /*stringDefault(buf,cursname);*/

    if (adefault == NULL)
	return;

#ifdef RFCURSORS
    cnum = XmuCursorNameToIndex(adefault);
    if (cnum != -1) {
	XColor  f, b;
	*cursor = XCreateFontCursor(W_Display, cnum);
	if (cnum == XC_xterm) {

	    f.pixel = colortable[W_Yellow].pixelValue;
	    b.pixel = colortable[W_Black].pixelValue;

	    XQueryColor(W_Display, W_Colormap, &f);
	    XQueryColor(W_Display, W_Colormap, &b);

	    XRecolorCursor(W_Display, *cursor, &f, &b);
	} else if (cnum == XC_pirate) {
	    f.pixel = colortable[W_Red].pixelValue;
	    b.pixel = colortable[W_Black].pixelValue;

	    XQueryColor(W_Display, W_Colormap, &f);
	    XQueryColor(W_Display, W_Colormap, &b);

	    XRecolorCursor(W_Display, *cursor, &f, &b);
	}
    } else
#endif
    if (0 == strcmp("bomb here", adefault)) {
	static Cursor bomb_here = 0;
	if (bomb_here == 0) {
	    bomb_here = make_cursor(cross_bits, crossmask_bits,
				    cross_width, cross_height,
				    cross_x_hot, cross_y_hot);
	}
	*cursor = bomb_here;
    } else if (0 == strcmp("tiny", adefault)) {
	static Cursor tiny = 0;
	if(tiny == 0) {
	    tiny = make_cursor(tiny_bits, tinymask_bits,
			       tiny_width, tiny_height,
			       tiny_x_hot, tiny_y_hot);
	}
	*cursor = tiny;
    }
}

static int
checkMapped(name)
    char   *name;
{
    char    buf[100];

    sprintf(buf, "%s.mapped", name);
    return 0;
}

#ifdef BUFFERING
static int
checkBuffered(name)
    char *name;
{
    char    buf[100];

    sprintf(buf, "%s.buffered", name);
#if 0
    /* defaults to on when in xpm mode for local window [BDyess] */
    /* don't turn it on unless asked when in OR mode [BDyess] */
    if(xpm && !useOR && strcmp(name,"local") == 0) {
      printf("Double Buffering automatically enabled.  Use -O if your machine can't handle it.\n");
      return (booleanDefault(buf, 1));
    } else {
      return (booleanDefault(buf, 0));
    }
#else
    return useBuffered;
#endif /*0*/
}
#endif /*BUFFERING [BDyess]*/

static void
addToHash(win)
    struct window *win;
{
    struct windowlist **new;

#ifdef DEBUG
    printf("Adding to %d\n", hash(win->window));
#endif
    new = &hashtable[hash(win->window)];
    while (*new != NULL) {
	new = &((*new)->next);
    }
    *new = (struct windowlist *) malloc(sizeof(struct windowlist));
    (*new)->next = NULL;
    (*new)->window = win;
}

struct window *
newWindow(window, type)
    Window  window;
    int     type;
{
    struct window *neww;

    neww = (struct window *) malloc(sizeof(struct window));
    neww->window = window;
    neww->drawable = window;
#ifdef BUFFERING
    neww->isbuffered = 0;
    neww->buffer = 0;
#endif /*BUFFERING [BDyess]*/
    neww->type = type;
    neww->mapped = 0;
    neww->insensitive = 0;
    addToHash(neww);
    return (neww);
}

void
W_RenameWindow(window, str)
    W_Window window;
    char   *str;
{
    XStoreName(W_Display, ((struct window *) window)->window, str);
}

  W_Window
w_MakeWindow(name, x, y, width, height, parent,
	     cursname, border, color, wsort)
    char   *name;
    int     x, y, width, height;
    W_Window parent;
    char   *cursname;
    int     border;
    W_Color color;		/* unused */
    int     wsort;		/* WIN_? */
{
    int     gx, gy;
    struct window *neww;
    Window  wparent;
    W_Window borderwin = 0;
    Cursor  cursor;
    XSetWindowAttributes attrs;
    unsigned int pwidth, pheight;	/* pixel width and height */

    if(wsort != WIN_BORDER) {
      checkGeometry(name, &gx, &gy, &width, &height);
      if (gx != INVALID_POSITION)
	  x = gx;
      if (gy != INVALID_POSITION)
	  y = gy;

      checkParent(name, &parent);

      if (wsort == WIN_TEXT || wsort == WIN_SCROLL || wsort == WIN_MENU) {
	  pwidth = width * W_Textwidth + WIN_EDGE * 2;
	  if (wsort == WIN_MENU)
	      pheight = height * (W_Textheight + MENU_PAD * 2 + MENU_BAR) - MENU_BAR;
	  else
	      pheight = height * W_Textheight + MENU_PAD * 2;
      } else {
	  pwidth = width;
	  pheight = height;
      }

      /* if this isn't a border, create one with another call to this 
	 function [BDyess] */
      if (border) {
#if 0
	char *newname = (char*) malloc (strlen(name) + 8);

	strcpy(newname,name);
	strcat(newname,"_border");
	free(newname);
#endif /*0*/
	parent = w_MakeWindow(name, x, y, pwidth+border*2, pheight+border*2, 
			      parent, cursname, border, color, WIN_BORDER);
	borderwin = parent;
	/* update the parameters to reflect the size of the surrounding burder
	   [BDyess] */
	x = border;
	y = border;
      }
      attrs.background_pixel = colortable[W_Black].pixelValue;
    } else { /* it is a border, set the background [BDyess] */
      pwidth = width;
      pheight = height;
      attrs.background_pixel = colortable[DARK_GREY].pixelValue;
    }

    wparent = W_Void2Window(parent)->window;

    checkCursor(name, cursname, &cursor);
    attrs.cursor = cursor;

    switch (wsort) {
    case WIN_TEXT:
    case WIN_MENU:
	attrs.event_mask = KeyPressMask | ButtonPressMask | ExposureMask | ButtonReleaseMask;
	attrs.do_not_propagate_mask = ExposureMask | KeyPressMask | ButtonPressMask;
	break;
    case WIN_GRAPH:
	attrs.event_mask = KeyPressMask | ButtonPressMask | ExposureMask | LeaveWindowMask | ButtonReleaseMask | ButtonMotionMask;
	attrs.do_not_propagate_mask = ExposureMask;
	break;
    case WIN_SCROLL:
	attrs.event_mask = ResizeRedirectMask | ExposureMask | KeyPressMask | ButtonReleaseMask | ButtonPressMask;
	attrs.do_not_propagate_mask = ResizeRedirectMask | ExposureMask;
	break;
    case WIN_BORDER:
        attrs.event_mask = ExposureMask;
	attrs.do_not_propagate_mask = ExposureMask;
	break;
    default:
	fprintf(stderr, "x11window.c: w_MakeWindow: unknown wsort %d\n", wsort);
    }

#ifdef AUTOKEY
    if (attrs.event_mask & KeyPressMask)
	attrs.event_mask |= KeyReleaseMask;
#endif				/* AUTOKEY */

    if (strcmp(name, "xgalaga_icon") == 0)	/* icon should not select for
						   input */
	attrs.event_mask = ExposureMask;
    if (strcmp(name, "wait_icon") == 0)	/* same here [BDyess] */
	attrs.event_mask = ExposureMask;

    if (strcmp(name, "info") == 0)	/* make info window passthru [BDyess] */
	attrs.event_mask = ExposureMask;

    neww = newWindow
	(XCreateWindow(W_Display, wparent, x, y, pwidth, pheight,
		      (unsigned) 0,
		       CopyFromParent, InputOutput, CopyFromParent,
		       (unsigned)(CWBackPixel | CWEventMask |
		       (cursor ? CWCursor : 0)),
		       &attrs),
	 wsort);

    neww->cursor = cursor;
    /* keep track of each windows border so they can be mapped and unmapped
       together. [BDyess] */
    neww->borderwin = borderwin;
    neww->border = border;
    neww->border_color = NONE;

    {
	char   *s;

	s = name;

	XStoreName(W_Display, neww->window, s);
    }

    wm_size_hint.width = wm_size_hint.min_width =
	wm_size_hint.max_width = wm_size_hint.base_width = pwidth;
    wm_size_hint.min_height = wm_size_hint.height =
	wm_size_hint.max_height = wm_size_hint.base_height = pheight;
    wm_size_hint.flags = USSize | PMinSize | PMaxSize | PBaseSize;
    if (gx > INVALID_POSITION || gy > INVALID_POSITION) {
	wm_size_hint.flags |= USPosition;
	wm_size_hint.x = x;
	wm_size_hint.y = y;
    }
    XSetWMNormalHints(W_Display, neww->window, &wm_size_hint);

    class_hint.res_name = name;
    class_hint.res_class = "XGalaga";
    XSetClassHint(W_Display, neww->window, &class_hint);

    XSetWMHints(W_Display, neww->window, &wm_hint);

    if (((wparent == W_Root &&
	  baseWin != NULL &&
	  strcmp(name, "wait") != 0)
	 || wsort == WIN_MENU) &&
	strcmp(name, "MetaServer List") != 0 &&
	strcmp(name, "Motd") != 0) {
	XSetTransientForHint(W_Display, neww->window,
			     W_Void2Window(baseWin)->window);
    }
    neww->name = strdup(name);
    neww->width = width;
    neww->height = height;
    if (wsort == WIN_MENU) {
	int     i;
	struct menuItem *items;
	items = (struct menuItem *) malloc(height * sizeof(struct menuItem));
	for (i = 0; i < height; i++) {
	    items[i].string = NULL;
	    items[i].color = W_White;
	    items[i].font = W_RegularFont;
	}
	neww->data = (char *) items;
    } else {
	neww->data = 0;
    }

    if (wparent != W_Root && wsort != WIN_BORDER)
	if (checkMapped(name))
	    W_MapWindow(W_Window2Void(neww));

#ifdef BUFFERING
    /* turn on buffering if name.buffered: on [BDyess] */
    if(wsort != WIN_BORDER) {
      if(checkBuffered(name)) {
	W_Buffer(W_Window2Void(neww), 1);
      }
    }
#endif /*BUFFERING [BDyess]*/

#ifdef DEBUG
    printf("New graphics window %d, child of %d\n", neww, parent);
#endif

#ifdef FOURPLANEFIX
    XSetWindowColormap(W_Display, neww->window, W_Colormap);
#endif

    return (W_Window2Void(neww));
}

W_Window
W_MakeWindow(name, x, y, width, height, parent, cursname, border, color)
    char   *name;
    int     x, y, width, height;
    W_Window parent;
    char   *cursname;
    int     border;
    W_Color color;
{
    return w_MakeWindow(name, x, y, width, height, parent,
			cursname, border, color, WIN_GRAPH);
}

W_Window
W_MakeTextWindow(name, x, y, width, height, parent, cursname, border)
    char   *name;
    int     x, y, width, height;
    W_Window parent;
    char   *cursname;
    int     border;
{
    return w_MakeWindow(name, x, y, width, height,
			parent, cursname, border, W_White, WIN_TEXT);
}



W_Window
W_MakeScrollingWindow(name, x, y, width, height, parent, cursname, border)
    char   *name;
    int     x, y, width, height;
    W_Window parent;
    char   *cursname;
    int     border;
{
    return w_MakeWindow(name, x, y, width, height, parent, cursname,
			border, W_White, WIN_SCROLL);
}

void
W_SetIconWindow(win, icon)
    W_Window win;
    W_Window icon;
{
    XWMHints hints;

    XSetIconName(W_Display, W_Void2Window(icon)->window, W_Void2Window(win)->name);

    hints.flags = IconWindowHint;
    hints.icon_window = W_Void2Window(icon)->window;
    XSetWMHints(W_Display, W_Void2Window(win)->window, &hints);
}



