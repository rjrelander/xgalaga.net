#define VERSION "2.0.34"

#define MAXTORPS 10
#define MINTORPS 3
#define TORPSPEED 12

#define ETORPSPEED 8

#define MINSPEED 3
#define MAXSPEED 8

#define TORPDELAY 5

#define WINWIDTH 400
#define WINHEIGHT 500

#define NUMSTARS 30

#define MAXALIENS 60

#define TURNSPEED 10

#define UTIMER 33333

#define BONUSSHIPSCORE 50000

#define ABS(a)			/* abs(a) */ (((a) < 0) ? -(a) : (a))

#define LEFTKEY 1
#define RIGHTKEY 2
#define FIREKEY 4

#ifndef HAVE_RANDOM
# ifndef HAVE_LRAND48
#  define random() rand()
# else
#  define random() lrand48()
# endif
#endif

#define NUMWEAPONS 3
#define SINGLESHOT 0
#define DOUBLESHOT 1
#define TRIPLESHOT 2

#define PRIZECHANCE 30

#define TORPCHANCE 60

#define SHIELDTIME 300

#define ALIENSHAPES 17
