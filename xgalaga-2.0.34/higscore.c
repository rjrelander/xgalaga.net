#include "Wlib.h"
#include "defs.h"
#include "struct.h"
#include "proto.h"
#include "data.h"

char new_name[20];
int nnpos;

undo_name()
{
    W_ClearArea(baseWin, 
		WINWIDTH/2-((strlen(new_name)/2)*W_Textwidth), WINHEIGHT/2,
		WINWIDTH/2+((strlen(new_name)/2)*W_Textwidth), W_Textheight);
}

do_name()
{
    char buf[21];

    sprintf(buf, "%s_", new_name);
    center_text(buf, WINHEIGHT/2, W_Cyan);
}

score_key(W_Event *ev);
{
    if(getting_name) {
	switch(ev->key) {
	  case 13:
	  case 10:
	    getting_name = 0;
	    break;
	  case 8:
	    if(nnpos > 0) {
		nnpos--;
		new_name[nnpos] = 0;
	    }
	    break;
	  case 'u'+128:
	    nnpos = 0;
	    new_name[nnpos] = 0;
	    break;
	  default:
	    if(nnpos < 19) {
		new_name[nnpos++] = ev->key;
	    }
	    break;
	}

	return 1;
    }
    return 0;
}

		
